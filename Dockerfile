FROM openjdk:17.0.2
ENV TZ=Asia/Baku
VOLUME /tmp
COPY build/libs/*.jar app.jar
ENTRYPOINT ["java"]
CMD [ "-jar", "./app.jar", "--spring.profiles.active=dev"]
