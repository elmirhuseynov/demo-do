#!/usr/bin/env sh
set -x
cd "$(dirname "$0")"
export env=${1:-dev}
export tag=${2:-$(git rev-parse --short HEAD)}

helm install $env demodo/ --namespace demodo-$env --set services.tag=$tag -f demodo/$env.yaml  --dry-run --debug

helm upgrade --install $env demodo/ --namespace demodo-$env --set services.tag=$tag -f demodo/$env.yaml

echo Release demo-do has been upgraded. Happy Helming!
